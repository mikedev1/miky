/*
 * Welcome to your app's main JavaScript file!
 *
 * We recommend including the built version of this JavaScript file
 * (and its CSS file) in your base layout (base.html.twig).
 */

// any CSS you import will output into a single css file (app.css in this case)
// start the Stimulus application
import "./bootstrap";
const $ = require("jquery");
// this "modifies" the jquery module: adding behavior to it
// the bootstrap module doesn't export/return anything
require("bootstrap");
import "./notyf";
import "./confetti";

import AOS from "aos";
import "aos/dist/aos";
import "aos/dist/aos.css";

import "./styles/app.scss";

AOS.init();

console.log(AOS);
