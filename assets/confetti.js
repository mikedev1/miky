import JSConfetti from "js-confetti";

const jsConfetti = new JSConfetti();

//jsConfetti.addConfetti();

jsConfetti.addConfetti({
  confettiRadius: 6,
  confettiNumber: 500,
});
