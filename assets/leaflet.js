//import L from "leaflet";

import "leaflet/dist/leaflet";
import 'leaflet/dist/leaflet.css';

var map = L.map('map').setView([49.159324, 1.604300], 19);

L.tileLayer('https://tile.openstreetmap.org/{z}/{x}/{y}.png', {
    maxZoom: 19,
    attribution: '&copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a>'
}).addTo(map);

var marker = L.marker([49.159324, 1.604300]).addTo(map);
/* var circle = L.circle([49.159324, 1.604300], {
    color: 'red',
    fillColor: '#f03',
    fillOpacity: 0.5,
    radius: 500
}).addTo(map); */

marker.bindPopup("<b>Coucou les amis ça va se passer ici</b><br>Let's get a dance").openPopup();
/* circle.bindPopup("I am a circle."); */
/* polygon.bindPopup("I am a polygon.");
 */
