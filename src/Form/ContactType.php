<?php

namespace App\Form;

use LDAP\Result;
use App\Entity\Contact;
use Symfony\Component\Form\AbstractType;
use FOS\CKEditorBundle\Form\Type\CKEditorType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints\NotNull;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\DomCrawler\Field\TextareaFormField;
use Symfony\Component\Form\Extension\Core\Type\RadioType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;

class ContactType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('prenom', TextType::class, [
                'row_attr' => [
                    'class' => 'form-floating mb-3'
                ],
                'attr' => [
                    'class' => 'form-control',
                    'placeholder' => 'Ton prénom'
                ],
                'label' => 'Prénom',
                'required' => false,
                'constraints' => [
                    new NotBlank(
                        ['message' => 'Tu es obligé de noter ton prénom  (^_^)']
                    )
                ]
            ])
            ->add('surnom', TextType::class, [
                'label' => 'Surnom, pour moi c\'est Le cuisiniste🤣',
                'row_attr' => [
                    'class' => 'form-floating mb-3'
                ],
                'attr' => [
                    'class' => 'form-control',
                    'placeholder' => 'Surnom: Le cuisiniste 🤣'
                ],
                'required' => false,

            ])
            ->add('text', TextareaType::class, [
                'label' => 'Ce champ est libre, je te laisse exprimer tes envies, tes souhaits, ton désir de t\'impliquer dans l\'évènement. Fais toi plaisir 🥰! Ah j\'oubliais si tu es accompagné(e) tu peux également noter le nom de la personne dans le champ ci-dessous',
                'required' => false,
                'attr' => [
                    'class' => 'form-control-lg'
                ],
                ])
            ->add('isHere',  ChoiceType::class, [
                'choices'  => [
                    'Yes I can! 🤪' => true,
                    'Non! malheuresement j\'ai piscine 🥶' => false,
                ],
                'label' => "J’accepte de participer à cet évènement et je m’engage solennellement à m’éclater sans réserve. 🎉🎊✨",
            ])
            ->add('Validation', SubmitType::class, [
                'label' => "Let's go to the party"
            ]);
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Contact::class,
        ]);
    }
}
