<?php

namespace App\Controller;

use App\Entity\Contact;
use App\Form\ContactType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class HomeController extends AbstractController
{
    #[Route('/', name: 'app_home')]
    public function index(Request $request, EntityManagerInterface $entityManager): Response
    {
        $contact = new Contact();

        $form = $this->createForm(ContactType::class, $contact);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            // $form->getData() holds the submitted values
            // but, the original `$task` variable has also been updated
            $contact = $form->getData();
            // ... perform some action, such as saving the task to the database

           // dd($contact);

           $entityManager->persist($contact);
           $entityManager->flush();

            $this->addFlash(
                'success',
                "Merci " . $contact->getPrenom() . " de t'être inscrit On se voit très vite kiss ❤"
            );
            return $this->redirectToRoute('app_plan');
        }

        return $this->render('home/index.html.twig', [
            'form' => $form
        ]);
    }

    #[Route('/plan', name: 'app_plan')]
    public function plan(): Response
    {
        return $this->render('home/plan.html.twig', []);
    }
}
